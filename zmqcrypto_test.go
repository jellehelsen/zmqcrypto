package main_test

import (
	"bitbucket.org/jellehelsen/zmqcrypto"
	"bitbucket.org/jellehelsen/zmqcrypto/config"
	"encoding/json"
	"encoding/hex"
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/zeromq/goczmq"
	"gopkg.in/h2non/gock.v1"
	"io/ioutil"
	"net/http"
	"testing"
	"time"
)

var infoResponse = map[string]interface{}{
	"result": map[string]interface{}{
		"version":         1000600,
		"protocolversion": 70916,
		"walletversion":   61000,
		"balance":         10148.67098778,
		"zerocoinbalance": 0.00000000,
		"blocks":          419013,
		"timeoffset":      0,
		"connections":     16,
		"proxy":           "",
		"difficulty":      9023.035548842105,
		"testnet":         false,
		"moneysupply":     13175918.07340848,
		"zLRMsupply": map[string]float32{
			"1":     0.00000000,
			"5":     0.00000000,
			"10":    0.00000000,
			"50":    0.00000000,
			"100":   0.00000000,
			"500":   0.00000000,
			"1000":  0.00000000,
			"5000":  0.00000000,
			"total": 0.00000000,
		},
		"keypoololdest":  1561229810,
		"keypoolsize":    1001,
		"paytxfee":       0.00000000,
		"relayfee":       0.00010000,
		"staking status": "Staking Active",
		"errors":         "",
	},
}

func methodMatcher(method string) gock.MatchFunc {
	return func(httpreq *http.Request, gockreq *gock.Request) (bool, error) {
		var data map[string]interface{}
		reader, err := httpreq.GetBody()
		body, err := ioutil.ReadAll(reader)
		err = json.Unmarshal(body, &data)
		if err != nil {
			return false, err
		}
		return data["method"] == method, nil
	}
}

func matchBlockHash(hash string) gock.MatchFunc {
	return func(httpreq *http.Request, gockreq *gock.Request) (bool, error) {
		var data map[string]interface{}
		reader, err := httpreq.GetBody()
		body, err := ioutil.ReadAll(reader)
		err = json.Unmarshal(body, &data)
		if err != nil {
			return false, err
		}
		return data["params"].([]interface{})[0] == hash, nil
	}
}

func TestZmqCrypto(t *testing.T) {
	fmt.Println("System Test")
	defer gock.Off()
	Convey("When running", t, func() {
		var conf *config.Config
		run := func() { main.Run(conf) }

		gock.New("http://testcoin:1234").
			Post("/").
			AddMatcher(methodMatcher("getinfo")).
			Reply(200).
			JSON(infoResponse)

		gock.New("http://testcoin:1234").
			Post("/").
			AddMatcher(methodMatcher("getblockhash")).
			Reply(200).
			JSON(map[string]string{"result": "abcde"})

		gock.New("http://testcoin:1234").
			Post("/").
			AddMatcher(methodMatcher("getblock")).
			AddMatcher(matchBlockHash("abcde")).
			Reply(200).
			JSON(map[string]interface{}{"result": map[string]interface{}{"hash": "abcde",
				"previousblockhash": "fghi",
				"tx": []string{"aabbccdd"},}})

		gock.New("http://testcoin:1234").
			Post("/").
			AddMatcher(methodMatcher("getblock")).
			AddMatcher(matchBlockHash("fghi")).
			Reply(200).
			JSON(map[string]interface{}{"result": map[string]string{"hash": "fghi", "previousblockhash": "fghi"}})

		gock.New("http://testcoin:1234").
			Post("/").
			AddMatcher(methodMatcher("getrawtransaction")).
			AddMatcher(matchBlockHash("aabbccdd")).
			Reply(200).
			JSON(map[string]interface{}{"result": map[string]interface{}{"hex": "aabbccdd",
				"txid": "aabbccdd",
				"version": 1234,
				"vin": []interface{}{struct{Txid string; Vout int}{Txid: "abcde", Vout: 1,}},
				"vout": []interface{}{struct{Value float32; N int; ScriptPubKey interface{}}{Value: 1234.56789, N: 1, ScriptPubKey: struct{Addresses []string}{Addresses: []string{"qewrt"},}}},
				"blockhash": "abcde",
				}})

		Convey("with an empty config", func() {
			conf = &config.Config{}
			Convey("zmqcrypto should panic", func() {
				So(run, ShouldPanic)
			})
		})
		Convey("with a config without coins", func() {
			conf = &config.Config{
				Databases: []config.Database{config.Database{"sql", "sqlite3", ":memory:"}},
			}
			run := func() { main.Run(conf) }
			Convey("zmqcrypto should panic", func() {
				So(run, ShouldPanic)
			})
		})
		Convey("with a config without databases", func() {
			conf = &config.Config{
				Coins: []config.Coin{config.Coin{
					Name: "testcoin",
					RPC:  config.RPC{"http://testcoin:1234", "testuser", "testpass"},
					ZMQ:  "inproc://testcoin",
				}},
			}
			Convey("zmqcrypto should panic", func() {
				So(run, ShouldPanic)
			})
		})
		Convey("with a config with an invalid database", func() {
			conf = &config.Config{
				Databases: []config.Database{config.Database{"non-existing", "non-existing", "non-existing"}},
				Coins: []config.Coin{config.Coin{
					Name: "testcoin",
					RPC:  config.RPC{"http://testcoin:1234", "testuser", "testpass"},
				}},
			}
			Convey("zmqcrypto should panic", func() {
				So(run, ShouldPanic)
			})
		})
		Convey("with a config with an invalid sql database", func() {
			conf = &config.Config{
				Databases: []config.Database{config.Database{"sql", "non-existing", "non-existing"}},
				Coins: []config.Coin{config.Coin{
					Name: "testcoin",
					RPC:  config.RPC{"http://testcoin:1234", "testuser", "testpass"},
				}},
			}
			Convey("zmqcrypto should panic", func() {
				So(run, ShouldPanic)
			})
		})
		Convey("with a valid config", func() {
			pub := goczmq.NewPubChanneler("inproc://testcoin")
			defer pub.Destroy()

			conf = &config.Config{
				Databases: []config.Database{config.Database{"sql", "sqlite3", ":memory:"}},
				Coins: []config.Coin{config.Coin{
					Name: "testcoin",
					RPC:  config.RPC{"http://testcoin:1234", "testuser", "testpass"},
					ZMQ:  "inproc://testcoin",
				}},
			}

			go func() {
				time.Sleep(1 * time.Second)
				main.Stop()
			}()

			Convey("zmqcrypto should not panic", func() {
				So(run, ShouldNotPanic)
			})

			Convey("zmqcrypto should have called rpc", func() {
				So(run, ShouldNotPanic)
				So(gock.IsDone(), ShouldBeTrue)
			})

			Convey("zmqcrypto should process the blocks received via zmq", func() {
				gock.New("http://testcoin:1234").
					Post("/").
					AddMatcher(methodMatcher("getblock")).
					AddMatcher(matchBlockHash("aabbcc")).
					Reply(200).
					JSON(map[string]interface{}{"result": map[string]string{"hash": "jklmn", "previousblockhash": "fghi"}})

				go func() {
					time.Sleep(200 * time.Millisecond)
					data, _ := hex.DecodeString("AABBCC")
					pub.SendChan <- [][]byte{[]byte("hashblock"), data}
				}()

				So(run, ShouldNotPanic)
				So(gock.IsDone(), ShouldBeTrue)
			})

			Reset(func() {
				gock.Off()
			})
		})
	})
}
