package db

import (
	"bitbucket.org/jellehelsen/zmqcrypto/config"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type SqlDB struct {
	DB *gorm.DB
}

func (db *SqlDB) Save(block Block) {
	db.DB.Create(&block)
}

func (db *SqlDB) BulkIndex(blocks []Block) {
	for _, block := range blocks {
		db.Save(block)
	}
}

func startSQL(config config.Database) DB {
	db, err := gorm.Open(config.Dialect, config.URL)
	if err != nil {
		logger.Panicf("failed to connect database: %s", err)
	}
	db.LogMode(false)
	db.AutoMigrate(&TxInput{}, &TxOutput{}, &Transaction{}, &Block{})
	return &SqlDB{db}
}

func init() {
	Register("sql", startSQL)
}
