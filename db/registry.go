package db

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 -generate

import (
	"bitbucket.org/jellehelsen/zmqcrypto/config"
	"fmt"
	"log"
	"os"
	"time"
)

//counterfeiter:generate . DB
type DB interface {
	Save(Block)
	BulkIndex([]Block)
}

type DBFactory func(config.Database) DB

type TxInput struct {
	ID       int64   `gorm:primary_key`
	Txid     *string `gorm:"type:varchar(64)"`
	TxRefer  string  `gorm:"type:varchar(64)"`
	Vout     int
	Sequence int64
}

type TxOutput struct {
	ID      int64  `gorm:primary_key`
	TxRefer string `gorm:"type:varchar(64)"`
	Value   float32
	N       int
	Type    string
	Address *string
}

type Transaction struct {
	Txid      string `gorm:"type:varchar(64);primary_key"`
	Blockhash string
	Hex       string
	Version   int
	Locktime  time.Time
	Time      time.Time
	Blocktime time.Time
	TxInputs  []TxInput  `gorm:"foreignkey:TxRefer"`
	TxOutputs []TxOutput `gorm:"foreignkey:TxRefer"`
}

type Block struct {
	Hash              string `gorm:"type:varchar(64);primary_key"`
	Confirmations     int
	Height            int
	Size              int
	Time              time.Time
	Previousblockhash string
	Transactions      []Transaction `gorm:"foreignkey:Blockhash"`

	CreatedAt time.Time
	UpdatedAt time.Time
}

var dbs = map[string]DBFactory{}

func Register(name string, db DBFactory) {
	dbs[name] = db
}

func Get(name string) DBFactory {
	factory, ok := dbs[name]
	if !ok {
		panic(fmt.Sprintf("%s not found", name))
	}
	return factory
}

var logger = log.New(os.Stdout, "", log.Ldate|log.Ltime)
