package db_test

import (
	"testing"
	"gopkg.in/h2non/gock.v1"
	"bitbucket.org/jellehelsen/zmqcrypto/db"
	"bitbucket.org/jellehelsen/zmqcrypto/config"

	. "github.com/smartystreets/goconvey/convey"
)

func TestElasticSave(t *testing.T){
	defer gock.Off()
	gock.New("http://localhost:9200").
		Put("/londinium-transactions").
			Reply(200)
	gock.New("http://localhost:9200").
		Post("/londinium-transactions/_doc").
			Reply(200)
	es := db.Get("elastic")(config.Database{})
	block := db.Block{Hash: "test"}
	Convey("Save the block", t, func(){
		es.Save(block)
		Convey("Elastic _doc should be called", func(){
			So(gock.IsDone(), ShouldBeTrue)
		})
	})
}

func TestElasticBulkIndex(t *testing.T){
	defer gock.Off()
	gock.New("http://localhost:9200").
		Put("/londinium-transactions").
			Reply(200)
	gock.New("http://localhost:9200").
		Post("/londinium-transactions/_bulk").
			Reply(200)

	es := db.Get("elastic")(config.Database{})
	blocks := []db.Block{{Hash: "test"}, {Hash: "test2"}}
	Convey("BulkIndex", t, func(){
		es.BulkIndex(blocks)
		Convey("Elastic _bulk should be called", func(){
			So(gock.IsDone(), ShouldBeTrue)
		})
	})
}
