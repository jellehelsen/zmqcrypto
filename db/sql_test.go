package db_test

import (
	"bitbucket.org/jellehelsen/zmqcrypto/config"
	"bitbucket.org/jellehelsen/zmqcrypto/db"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestGORMSave(t *testing.T) {
	block := db.Block{Hash: "test"}
	gdb, _ := gorm.Open("sqlite3", ":memory:")
	defer gdb.Close()
	gdb.AutoMigrate(&db.TxInput{}, &db.TxOutput{}, &db.Transaction{}, &db.Block{})
	sqldb := &db.SqlDB{gdb}
	Convey("When saving the block", t, func() {
		sqldb.Save(block)
		Convey("It should have 1 block in the db", func() {
			var count int
			gdb.Model(&db.Block{}).Count(&count)
			So(count, ShouldEqual, 1)
		})
	})
}

func TestGORMBulkIndex(t *testing.T) {
	Convey("BulkIndex", t, func() {
		blocks := []db.Block{{Hash: "test"}, {Hash: "test2"}, {Hash: "test3"}}
		gdb, _ := gorm.Open("sqlite3", ":memory:")
		defer gdb.Close()
		gdb.AutoMigrate(&db.TxInput{}, &db.TxOutput{}, &db.Transaction{}, &db.Block{})
		sqldb := &db.SqlDB{gdb}
		Convey("When saving 3 blocks", func() {
			sqldb.BulkIndex(blocks)
			Convey("3 blocks should be saved", func() {
				var count int
				gdb.Model(&db.Block{}).Count(&count)
				So(count, ShouldEqual, 3)
			})
		})
	})
}

func TestGORMFactory(t *testing.T) {
	Convey("With a sqlite3 memory config", t, func() {
		config := config.Database{"sql", "sqlite3", ":memory:"}
		Convey("When you create the DB", func() {
			sqldb := db.Get("sql")(config).(*db.SqlDB)
			defer sqldb.DB.Close()
			Convey("Then the database should be of the correct type", func() {
				So(sqldb.DB.Dialect().GetName(), ShouldEqual, "sqlite3")
			})
		})
	})
}
