package db

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"
	"github.com/elastic/go-elasticsearch/v8/estransport"
	"github.com/elastic/go-elasticsearch/v8/esutil"
	"os"
	"strings"
	"bitbucket.org/jellehelsen/zmqcrypto/config"
)

type Elastic struct {
	es *elasticsearch.Client
}

func (elastic *Elastic) Save(block Block) {
	req := esapi.IndexRequest{
		Index:   "londinium-transactions",
		Body:    esutil.NewJSONReader(block),
		Refresh: "true",
	}
	res, err := req.Do(context.Background(), elastic.es)
	if err != nil {
		logger.Panicf("Error saving to elastic: %s", err)
	}
	defer res.Body.Close()
}

func (elastic *Elastic) BulkIndex(blocks []Block) {
	var buf bytes.Buffer
	for _, block := range blocks {
		meta := []byte(fmt.Sprintf(`{ "index" : { "_id" : "%s" } }%s`, block.Hash, "\n"))
		data, err := json.Marshal(block)
		if err != nil {
			logger.Fatalf("Cannot encode transaction %s: %s", block.Hash, err)
		}
		data = append(data, "\n"...)
		buf.Grow(len(meta) + len(data))
		buf.Write(meta)
		buf.Write(data)
	}

	for i := 1; i < 10; i++ {
		res, err := elastic.es.Bulk(bytes.NewReader(buf.Bytes()),
			elastic.es.Bulk.WithIndex("londinium-transactions"))
		if err == nil {
			defer res.Body.Close()
			break
		}
	}
}

func startElastic(config config.Database) DB {
	mapping := `{
		"settings": {
			"index.number_of_replicas": 0
		},
		"mappings": {
				"properties": {
					"Time":  { "type": "date", "format": "epoch_second" },
					"Vin":  { "type": "nested"},
					"Vout":  { "type": "nested", "properties": {
						"ScriptPubKey" : { "type": "nested" }
					}}
				}
		}
		}`

	es, err := elasticsearch.NewClient(elasticsearch.Config{
		Logger: &estransport.ColorLogger{Output: os.Stdout},
	})

	if err != nil {
		logger.Panicf("Error starting elastic: %s", err)
	}
	es.Indices.Create("londinium-transactions", es.Indices.Create.WithBody(strings.NewReader(mapping)))
	return &Elastic{es: es}
}

func init() {
	Register("elastic", startElastic)
}
