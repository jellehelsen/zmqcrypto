package worker

import (
	"bitbucket.org/jellehelsen/zmqcrypto/db"
	"bitbucket.org/jellehelsen/zmqcrypto/rpc"
	"bitbucket.org/jellehelsen/zmqcrypto/zmq"
	"log"
)

type Worker struct {
	zmqClient zmq.Client
	rpcClient rpc.Client
	closing   chan bool
}

func NewWorker(zmq zmq.Client, rpc rpc.Client) (worker *Worker) {
	worker = &Worker{zmq, rpc, make(chan bool)}
	return
}

func (w *Worker) Subscribe(client rpc.Client, db db.DB, done chan bool) {
	recvChan := w.zmqClient.Subscribe("hashblock")
loop:
	for {
		select {
		case blockhash, ok := <-recvChan:
			if !ok {
				break loop
			}
			block, err := client.GetBlock(blockhash)
			if err != nil {
				panic(err)
			}
			db.Save(TransformBlock(block))
		case <-w.closing:
			break loop
		}
	}
	close(done)
}

func (w *Worker) Index(client rpc.Client, dbs []db.DB) {
	height, err := client.GetBlockHeight()
	if err != nil {
		log.Panicf("Error getting block height: %s", err)
	}
	hash, err := client.GetBlockHash(height)
	if err != nil {
		log.Panicf("Error get block hash : %s", err)
	}

	blocks := make(chan rpc.Block, 100)

	go func() {
		lblk := make([]db.Block, 0)
		for blk := range blocks {
			lblk = append(lblk, TransformBlock(blk))
			if len(lblk) == 100 {
				for _, cdb := range dbs {
					cdb.BulkIndex(lblk)
				}
				lblk = make([]db.Block, 0)
			}
		}
		if len(lblk) > 0 {
			for _, cdb := range dbs {
				cdb.BulkIndex(lblk)
			}
		}
	}()

	for {
		block, err := client.GetBlock(hash)
		if err != nil {
			log.Panicf("Unable to get block: %s", err)
		}

		blocks <- block

		if block.Hash == block.Previousblockhash {
			close(blocks)
			break
		}
		hash = block.Previousblockhash
	}
}

func (w *Worker) Stop() {
	close(w.closing)
	// w.zmqClient.Close()
}
