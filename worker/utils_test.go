package worker_test

import (
	"encoding/json"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	// . "bitbucket.org/jellehelsen/zmqcrypto"
	"bitbucket.org/jellehelsen/zmqcrypto/rpc"
	"bitbucket.org/jellehelsen/zmqcrypto/worker"
)

const mockTransaction = `{
	"hex": "test",
	"txid": "txid",
	"vin": [{
		"txid": "vintxid",
		"vout": 0
	}],
	"vout": [{
		"value": 1.2,
		"n": 1,
		"ScriptPubKey": {
			"addresses": ["abcd"]
		}
	}]
}`
func TestTransformBlock(t *testing.T) {
	var tx rpc.Transaction
	err := json.Unmarshal([]byte(mockTransaction), &tx)

	Convey("When you transform an RPC.Block", t, func() {
		So(err, ShouldBeNil)
		rpcBlock := rpc.Block{Hash: "abcd", Transactions: []rpc.Transaction{tx}}
		dbBlock := worker.TransformBlock(rpcBlock)
		Convey("The DB block should have the same values", func(){
			So(dbBlock.Hash, ShouldEqual, rpcBlock.Hash)
			So(dbBlock.Transactions, ShouldNotBeEmpty)
		})

	})

}
