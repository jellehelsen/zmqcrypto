package worker_test

import (
	"testing"
	"time"
	"fmt"
	"errors"
	this "bitbucket.org/jellehelsen/zmqcrypto/worker"
	zmq "bitbucket.org/jellehelsen/zmqcrypto/zmq/zmqfakes"
	"bitbucket.org/jellehelsen/zmqcrypto/rpc/rpcfakes"
	"bitbucket.org/jellehelsen/zmqcrypto/rpc"
	"bitbucket.org/jellehelsen/zmqcrypto/db"
	"bitbucket.org/jellehelsen/zmqcrypto/db/dbfakes"
	. "github.com/smartystreets/goconvey/convey"
)


func TestWorker(t *testing.T){
	Convey("Worker", t, func(){
		rpcClient := &rpcfakes.FakeClient{}
		zmqClient := &zmq.FakeClient{}
		worker := this.NewWorker(zmqClient, rpcClient)
		fakeDB := &dbfakes.FakeDB{}
		Convey("When subscribing", func(){
			zmqChannel := make(chan string)
			zmqClient.SubscribeReturns(zmqChannel)
			done := make(chan bool)
			Convey("when a blockhash was received", func(){
				go func(){
					time.Sleep(2*time.Millisecond)
					zmqChannel <- "abcde"
					time.Sleep(20*time.Millisecond)
					close(zmqChannel)
				}()
				Convey("then the block should be fetched from rpc", func(){
					waiter := func(){
						select {
						case <- done:
						case <- time.After(time.Second * 5):
							panic("message not received within 5 seconds")
						}
					}
					FocusConvey("and should be saved to the DB", func(){
						go worker.Subscribe(rpcClient, fakeDB, done)
						waiter()
						// So(waiter, ShouldNotPanic)
						So(rpcClient.GetBlockCallCount(), ShouldEqual, 1)
						So(rpcClient.GetBlockArgsForCall(0), ShouldEqual, "abcde")
						So(fakeDB.SaveCallCount(), ShouldEqual, 1)
					})
					Convey("when rpc fails", func(){
						subscribe := func(){
							worker.Subscribe(rpcClient, fakeDB, done)
						}
						rpcClient.GetBlockReturns(rpc.Block{}, errors.New("some error"))
						go waiter()
						Convey("then subscribe should panic", func(){
							So(subscribe, ShouldPanic)
						})
						Convey("and the block is not saved", func(){
							So(fakeDB.SaveCallCount(), ShouldEqual, 0)
						})
					})
				})
			})
		})
		Convey("When indexing", func(){
			index := func(){
				worker.Index(rpcClient, []db.DB{fakeDB})
			}
			Convey("When getting the block height is successfull", func(){
				rpcClient.GetBlockHeightReturns(3, nil)
				Convey("When getting the block hash is successfull", func(){
					rpcClient.GetBlockHashReturns("ABCD", nil)
					Convey("When getting the block is successfull", func(){
						counter := 1000
						rpcClient.GetBlockStub = func(hash string) (rpc.Block, error){
							next := counter - 1
							if next <= 880 {
								next = counter
							}
							block := rpc.Block{
								Hash: fmt.Sprintf("%d", counter),
								Previousblockhash: fmt.Sprintf("%d", next),
							}
							counter--
							return block, nil
						}
						Convey("indexing should not panic", func(){
							So(index, ShouldNotPanic)
						})
						Convey("all block should be saved to the DB", func(){
							So(fakeDB.BulkIndexCallCount(), ShouldEqual, 0)
							index()
							time.Sleep(1*time.Second)
							So(fakeDB.BulkIndexCallCount(), ShouldEqual, 2)
							So(len(fakeDB.BulkIndexArgsForCall(0)), ShouldEqual, 100)
							So(len(fakeDB.BulkIndexArgsForCall(1)), ShouldEqual, 20)
						})
					})
					Convey("When getting the block is unsuccessfull", func(){
						rpcClient.GetBlockReturns(rpc.Block{}, errors.New("Could not get block"))
						Convey("indexing should panic", func(){
							So(index, ShouldPanic)
						})
					})
				})
				Convey("When getting the block hash is unsuccessfull", func(){
					rpcClient.GetBlockHashReturns("", errors.New("Could not get hash"))
					Convey("indexing should panic", func(){
						So(index, ShouldPanic)
					})
				})
			})
			Convey("When getting the block height is unsuccessfull", func(){
				rpcClient.GetBlockHeightReturns(0, errors.New("Could not get block height"))
				Convey("indexing should panic", func(){
					So(index, ShouldPanic)
				})
			})
		})
	})
}
