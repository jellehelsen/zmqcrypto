package worker

import (
	"time"
	"bitbucket.org/jellehelsen/zmqcrypto/db"
	"bitbucket.org/jellehelsen/zmqcrypto/rpc"
)


// TransformBlock transforms a block from RPC to the correct format for DB storage
func TransformBlock(rpcBlock rpc.Block) (dbBlock db.Block) {
	transactions := make([]db.Transaction, 0)

	for _, tx := range rpcBlock.Transactions {
		inputs := make([]db.TxInput, 0)
		outputs := make([]db.TxOutput, 0)
		for _, vin := range tx.Vin {
			inputs = append(inputs, db.TxInput{
				Txid:     vin.Txid,
				Vout:     vin.Vout,
				Sequence: vin.Sequence})
		}
		for _, vout := range tx.Vout {
			var address *string
			if len(vout.ScriptPubKey.Addresses) > 0 {
				address = &vout.ScriptPubKey.Addresses[0]
			}
			outputs = append(outputs, db.TxOutput{
				Value:   vout.Value,
				N:       vout.N,
				Type:    vout.ScriptPubKey.Type,
				Address: address})
		}

		transaction := db.Transaction{
			Txid:      tx.Txid,
			Blockhash: tx.Blockhash,
			Hex:       tx.Hex,
			Version:   tx.Version,
			Locktime:  time.Unix(tx.Locktime, 0),
			Time:      time.Unix(tx.Time, 0),
			Blocktime: time.Unix(tx.Blocktime, 0),
			TxInputs:  inputs,
			TxOutputs: outputs,
		}
		transactions = append(transactions, transaction)
	}

	dbBlock = db.Block{
		rpcBlock.Hash,
		rpcBlock.Confirmations,
		rpcBlock.Height,
		rpcBlock.Size,
		time.Unix(rpcBlock.Time, 0),
		rpcBlock.Previousblockhash,
		transactions,
		time.Now(),
		time.Now(),
	}

	return
}
