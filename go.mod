module bitbucket.org/jellehelsen/zmqcrypto

go 1.12

require (
	github.com/elastic/go-elasticsearch v0.0.0 // indirect
	github.com/elastic/go-elasticsearch/v8 v8.0.0-20190910075000-5963931cb201
	github.com/golang/mock v1.2.0
	github.com/gopherjs/gopherjs v0.0.0-20190910122728-9d188e94fb99 // indirect
	github.com/jinzhu/gorm v1.9.10
	github.com/maxbrunsfeld/counterfeiter/v6 v6.2.2
	github.com/smartystreets/assertions v1.0.1 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
	github.com/spf13/viper v1.4.0
	github.com/zeromq/goczmq v4.1.0+incompatible
	golang.org/x/crypto v0.0.0-20190911031432-227b76d455e7 // indirect
	golang.org/x/net v0.0.0-20190912160710-24e19bdeb0f2 // indirect
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	golang.org/x/sys v0.0.0-20190913121621-c3b328c6e5a7 // indirect
	golang.org/x/tools v0.0.0-20190912215617-3720d1ec3678 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/h2non/gock.v1 v1.0.15
)
