package zmq

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 -generate

import (
	"encoding/hex"
	"github.com/zeromq/goczmq"
	// "log"
)

// UpdateHandler handles all zmq updates
type UpdateHandler func(string)

//counterfeiter:generate . Client
// Client is the interface
type Client interface {
	Subscribe(string) chan string
	Close()
}

// ZmqClient is the implementation
type client struct {
	channeler *goczmq.Channeler
}

// New returns a new zmq client
func New(url string) Client {
	return &client{goczmq.NewSubChanneler(url)}
}

// Subscribe to the given zmq queue
func (client *client) Subscribe(topic string) chan string {
	data := make(chan string)
	client.channeler.Subscribe(topic)
	go client.consume(data)
	return data
}

func (client *client) consume(data chan string) {
	for msg := range client.channeler.RecvChan {
		hash := hex.EncodeToString(msg[1])
		data <- hash
	}
}

// Close the channel
func (client *client) Close() {
	client.channeler.Destroy()
}
