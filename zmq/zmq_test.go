package zmq_test

import (
	"testing"
	"time"
	"encoding/hex"
	"bitbucket.org/jellehelsen/zmqcrypto/zmq"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/zeromq/goczmq"
)

func TestZmq(t *testing.T){
	pub := goczmq.NewPubChanneler("inproc://testchannel")
	defer pub.Destroy()
	client := zmq.New("inproc://testchannel")
	defer client.Close()
	Convey("When subscribing", t, func(){
		recvChan := client.Subscribe("test-topic")
		time.Sleep(1*time.Millisecond)
		Convey("when sending a test message", func(){
			go func(){
				pub.SendChan <- [][]byte{[]byte("test-topic"),[]byte("a test message")}
			}()
			Convey("the test message should be received", func(){
				select {
				case data := <- recvChan:
					msg, err := hex.DecodeString(data)
					So(err, ShouldBeNil)
					So(string(msg), ShouldEqual, "a test message")
				case <- time.After(time.Second * 5):
					t.Errorf("message not received within 5 seconds")
				}
			})
		})
	})
}
