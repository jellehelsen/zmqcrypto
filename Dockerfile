FROM golang:alpine AS build
MAINTAINER Jelle Helsen <jelle@hcode.be>


RUN apk add alpine-sdk czmq-dev
copy go.* /go/src/zmqcrypto/
WORKDIR /go/src/zmqcrypto
run go mod download
COPY . /go/src/zmqcrypto
RUN go build

FROM scratch
COPY --from=build /go/src/zmqcrypto/zmqcrypto /
CMD ["/zmqcrypto"]
