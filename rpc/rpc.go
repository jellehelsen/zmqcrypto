package rpc
//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 -generate

import (
	"encoding/json"
	// "log"
	// "fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type Info struct {
	Version int
	Blocks  int
}

type InfoResponse struct {
	Result Info
}

//counterfeiter:generate . Client
type Client interface {
	GetBlockHeight() (int, error)
	GetBlockHash(int) (string, error)
	GetBlock(string) (Block, error)
	GetTransaction(string) (Transaction, error)
}

type client struct {
	Address  string
	User     string
	Password string
	Http     *http.Client
	lastID   int
}

type GetBlockHashResponse struct {
	Result string
}

type Block struct {
	Confirmations     int
	Difficulty        float64
	Hash              string
	Height            int
	Version           int
	Time              int64
	Size              int
	Previousblockhash string
	Tx                []string
	Transactions      []Transaction
}

type GetBlockResponse struct {
	Result Block
}

type Transaction struct {
	Hex      string
	Txid     string
	Version  int
	Locktime int64
	Vin      []struct {
		Txid      *string
		Vout      int
		ScriptSig struct {
			Asm string
			Hex string
		}
		Sequence int64
	}
	Vout []struct {
		Value        float32
		N            int
		ScriptPubKey struct {
			Asm       string
			Hex       string
			ReqSigs   int
			Type      string
			Addresses []string
		}
	}
	Blockhash     string
	Confirmations int
	Time          int64
	Blocktime     int64
}

type GetTransactionResponse struct {
	Result Transaction
}

func Dial(address, username, password string) Client {
	client := client{address, username, password, nil, 0}
	client.Http = &http.Client{}
	return &client
}

func (client *client) call(method string, params interface{}, result interface{}) error {
	client.lastID++
	data, err := json.Marshal(map[string]interface{}{
		"method": method,
		"id":     client.lastID,
		"params": params,
	})
	if err != nil {
		return err
	}
	// log.Println(data)
	req, err := http.NewRequest("POST", client.Address,
		strings.NewReader(string(data)))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	// log.Println(req)
	req.SetBasicAuth(client.User, client.Password)
	resp, err := client.Http.Do(req)
	if err != nil {
		return err
	}
	// log.Println(resp)
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	// fmt.Println(string(body))
	err = json.Unmarshal(body, result)
	// fmt.Println(result)
	return err
}

func (client *client) GetBlockHeight() (int, error) {
	info := InfoResponse{}
	// fmt.Println("GetBlockHeight")
	err := client.call("getinfo", nil, &info)
	// fmt.Println(info)
	// fmt.Println(err)
	return info.Result.Blocks, err
}

func (client *client) GetBlockHash(height int) (string, error) {
	var hash GetBlockHashResponse
	err := client.call("getblockhash", []int{height}, &hash)
	return hash.Result, err
}

func (client *client) GetBlock(hash string) (Block, error) {
	var blockResponse GetBlockResponse
	err := client.call("getblock", []string{hash}, &blockResponse)
	for _, tx := range blockResponse.Result.Tx {
		transaction, err := client.GetTransaction(tx)
		if err != nil {
			return blockResponse.Result, err
		}
		blockResponse.Result.Transactions = append(blockResponse.Result.Transactions, transaction)
	}
	return blockResponse.Result, err
}

func (client *client) GetTransaction(hash string) (Transaction, error) {
	// fmt.Println(hash)
	var response GetTransactionResponse
	err := client.call("getrawtransaction", []interface{}{hash, 1}, &response)
	return response.Result, err
}
