package rpc_test

import (
	"bitbucket.org/jellehelsen/zmqcrypto/rpc"
	. "github.com/smartystreets/goconvey/convey"
	"gopkg.in/h2non/gock.v1"
	"testing"
)

var infoResponse = map[string]interface{}{
	"result": map[string]interface{}{
		"version":         1000600,
		"protocolversion": 70916,
		"walletversion":   61000,
		"balance":         10148.67098778,
		"zerocoinbalance": 0.00000000,
		"blocks":          419013,
		"timeoffset":      0,
		"connections":     16,
		"proxy":           "",
		"difficulty":      9023.035548842105,
		"testnet":         false,
		"moneysupply":     13175918.07340848,
		"zLRMsupply": map[string]float32{
			"1":     0.00000000,
			"5":     0.00000000,
			"10":    0.00000000,
			"50":    0.00000000,
			"100":   0.00000000,
			"500":   0.00000000,
			"1000":  0.00000000,
			"5000":  0.00000000,
			"total": 0.00000000,
		},
		"keypoololdest":  1561229810,
		"keypoolsize":    1001,
		"paytxfee":       0.00000000,
		"relayfee":       0.00010000,
		"staking status": "Staking Active",
		"errors":         "",
	},
}

var blockResponse = map[string]rpc.Block{
	"result": rpc.Block{
		Hash: "abcd",
		Tx:   []string{"efgh"},
	}}

var transactionResponse = rpc.Transaction{Hex: "efgh"}

func TestGetBlockHeight(t *testing.T) {
	defer gock.Off()
	// gock.Observe(gock.DumpRequest)

	client := rpc.Dial("http://londinium:1234", "user", "password")
	Convey("When getting the block height", t, func() {
		infoResponse["result"].(map[string]interface{})["blocks"] = 10
		gock.New("http://londinium:1234").
			Post("/").
			MatchHeader("Authorization", "Basic dXNlcjpwYXNzd29yZA==").
			MatchType("json").
			BodyString("getinfo").
			Reply(200).
			JSON(infoResponse)
		height, err := client.GetBlockHeight()
		Convey("it should not return an error", func() {
			So(err, ShouldBeNil)
		})
		Convey("the height should match", func() {
			So(height, ShouldEqual, 10)
		})
	})
}

func TestGetBlockHash(t *testing.T) {
	defer gock.Off()

	client := rpc.Dial("http://londinium:1234", "user", "password")
	Convey("When getting the block hash", t, func() {
		gock.New("http://londinium:1234").
			Post("/").
			MatchHeader("Authorization", "Basic dXNlcjpwYXNzd29yZA==").
			MatchType("json").
			BodyString("getblockhash").
			Reply(200).
			JSON(map[string]string{"result": "abcd"})
		hash, err := client.GetBlockHash(1)
		Convey("it should not return an error", func() {
			So(err, ShouldBeNil)
		})
		Convey("the height should match", func() {
			So(hash, ShouldEqual, "abcd")
		})
	})
}

func TestGetBlock(t *testing.T) {
	defer gock.Off()

	// gock.Observe(gock.DumpRequest)
	client := rpc.Dial("http://londinium:1234", "user", "password")
	Convey("When getting the block hash", t, func() {
		gock.New("http://londinium:1234").
			Post("/").
			MatchHeader("Authorization", "Basic dXNlcjpwYXNzd29yZA==").
			MatchType("json").
			BodyString("getblock").
			Reply(200).
			JSON(blockResponse)
		gock.New("http://londinium:1234").
			Post("/").
			MatchHeader("Authorization", "Basic dXNlcjpwYXNzd29yZA==").
			MatchType("json").
			BodyString("getrawtransaction").
			Reply(200).
			JSON(transactionResponse)
		block, err := client.GetBlock("abcd")
		Convey("it should not return an error", func() {
			So(err, ShouldBeNil)
		})
		Convey("the hash should match", func() {
			So(block.Hash, ShouldEqual, "abcd")
		})
	})
}

func TestErrorHandling(t *testing.T) {
	Convey("When an bad url is given", t, func() {
		client := rpc.Dial("an-invalid-url", "user", "password")
		hash, err := client.GetBlockHash(1)
		Convey("it should return an error", func() {
			So(err, ShouldNotBeNil)
		})
		Convey("the hash should be empty", func() {
			So(hash, ShouldEqual, "")
		})
	})
	Convey("When a HTTP error is returned", t, func() {
		gock.Observe(gock.DumpRequest)
		client := rpc.Dial("http://londinium:1234", "user", "password")
		gock.New("http://londinium:1234").
			Post("/").
			MatchHeader("Authorization", "Basic dXNlcjpwYXNzd29yZA==").
			MatchType("json").
			BodyString("getblock").
			Reply(404)
		block, err := client.GetBlock("abcd")
		Convey("it should return an error", func() {
			So(err, ShouldNotBeNil)
		})
		Convey("the block should be empty", func() {
			So(block.Hash, ShouldEqual, "")
		})
	})
}
