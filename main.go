package main

import (
	"bitbucket.org/jellehelsen/zmqcrypto/config"
	"bitbucket.org/jellehelsen/zmqcrypto/db"
	"bitbucket.org/jellehelsen/zmqcrypto/rpc"
	"bitbucket.org/jellehelsen/zmqcrypto/worker"
	"bitbucket.org/jellehelsen/zmqcrypto/zmq"
)

var workers []*worker.Worker

func Run(config *config.Config) {
	var dbs []db.DB
	workers = make([]*worker.Worker, 0)
	done := make(chan bool)
	databases := config.Databases
	coins := config.Coins
	if len(databases) == 0 {
		panic("No databases in configuration")
	}
	if len(coins) == 0 {
		panic("No coins in configuration")
	}
	for _, database := range databases {
		thisdb := db.Get(database.Driver)(database)
		dbs = append(dbs, thisdb)
	}

	for _, coin := range coins {
		client := rpc.Dial(coin.RPC.Endpoint, coin.RPC.User, coin.RPC.Password)
		sub := zmq.New(coin.ZMQ)
		defer sub.Close()
		worker := worker.NewWorker(sub, client)
		workers = append(workers, worker)
		go worker.Index(client, dbs)
		if coin.ZMQ != "" {
			for _, thisdb := range dbs {
				go worker.Subscribe(client, thisdb, done)
			}
		} else {
			close(done)
		}
	}

	<-done
}

func Stop() {
	for _, worker := range workers {
		worker.Stop()
	}
}

func main() {
	config, _ := config.New()
	Run(config)
}
