package config

import (
	"github.com/spf13/viper"
	// "log"
)

type Database struct {
	Driver  string
	Dialect string
	URL     string
}

type RPC struct {
	Endpoint string
	User     string
	Password string
}

type Coin struct {
	Name string
	RPC
	ZMQ string
}

type Config struct {
	Databases []Database
	Coins     []Coin
}

func New() (*Config, error) {
	cfg := &Config{}
	viper.New()
	viper.SetConfigType("yml")
	viper.SetConfigName("config")
	viper.AddConfigPath("./config")
	viper.AddConfigPath("./")

	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	if err := viper.Unmarshal(cfg); err != nil {
		return nil, err
	}
	return cfg, nil
}
